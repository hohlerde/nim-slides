# nim-slides
This slide deck is an introduction to the programming language [Nim](http://nim-lang.org). It uses the fantastic [reveal.js](https://github.com/hakimel/reveal.js) framework written by Hakim El Hattab.

In the file `index.html` I commented out some slides due to timing constraints I had for my talk, namely "Concepts", "Foreign Function Interface" and "Static Duck Typing".

## Folders
Under `videos` you'll find some HD videos used in the presentation. They are the main reason why the slide deck needs around 94 mb on disk.

Folder `nimex` contains Nim code snippets/examples you'll find on some slides and `images` contains all images.

## Tools
The svg graphics were created with [EDraw](https://www.edrawsoft.com/) and [Draw.io](https://www.draw.io/). Edraw uses edx and Draw.io xml files. You'll find those in the `images` folder as well.
