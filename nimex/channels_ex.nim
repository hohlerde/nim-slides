import os, threadpool

var chan: Channel[string]
open(chan)

proc sayHello() =
  sleep(10000)
  chan.send("Hello!")

spawn sayHello()

echo(chan.recv()) 
