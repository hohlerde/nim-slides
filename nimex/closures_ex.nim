type
  ILinuxDistro = tuple[
    setName: proc(v: string) {.closure.},
    getName: proc(): string {.closure.}]

proc getInterf(): ILinuxDistro =
  var name: string

  let setter = proc (v: string) =
    name = v

  let getter = proc(): string =
    return name

  return (setName: setter, getName: getter)


var obj = getInterf()
obj.setName("Fedora")

echo obj.getName()
