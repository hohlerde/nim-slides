type
  CanDance = concept x
    dance(x)

  Person = object
  Robot = object

proc dance(p: Person) =
  echo "People can dance, but not Robots!"

proc doBalletConcept(dancer: CanDance) =
  dance(dancer)

let p = Person()
let r = Robot()

doBalletConcept(p) # works. prints: "People can dance, but not Robots!"
doBalletConcept(r) # Error: type mismatch: got (Robot)

proc doBalletGeneric[T](dancer: T) =
  dance(dancer)

doBalletGeneric(p) # works. prints: "People can dance, but not Robots!"
doBalletGeneric(r)
