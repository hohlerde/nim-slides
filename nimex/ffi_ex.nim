when defined(linux):
  const dllname = "libusb.so"
elif defined(freebsd):
  const dllname = "libusb.so"
elif defined(macosx):
  const dllname = "libusb.dylib"
elif defined(windows):
  const dllname = "libusb-1.0.dll"
else:
  {.error: "libusb does not support this platform".}

type
  LibusbVer* {.importc: "struct libusb_version", header: "<libusb.h>".} = object
    major*: uint16
    minor*: uint16
    micro*: uint16

  LibusbCtx* = object

proc libusbInit*(ctx: ptr ptr LibusbCtx): cint
  {.cdecl, dynlib: dllname, importc: "libusb_init".}

proc libusbExit*(ctx: ptr LibusbCtx)
  {.cdecl, dynlib: dllname, importc: "libusb_exit".}

proc libusbGetVer(): ptr LibusbVer
  {.cdecl, dynlib: dllname, importc: "libusb_get_version".}


echo("Init: ", libusbInit(nil))

var pver = libusbGetVer()
echo(repr(pver))

libusbExit(nil)
