proc showNumber[T: int | float](n: T) =
  echo(n)

showNumber(42)
showNumber(12.3)
