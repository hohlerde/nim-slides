type
  Simple* = ref object
    mass* : float
    speed*: float

  Composed* = ref object

proc energy[T](obj: T): float =
  return 0.5 * obj.mass * obj.speed * obj.speed

proc mass*(co: Composed): float =
  return 42

proc speed*(co: Composed): float =
  return 0815

var simple = Simple(mass: 1.0, speed: 5.0)
echo simple.energy()

var composed = Composed()
echo composed.energy()
