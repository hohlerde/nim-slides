type
  EGender* = enum
    Male,
    Female

  Animal* = ref object of RootObj
    name*  : string
    gender*: EGender

  Dog*    = ref object of Animal
  Cat*    = ref object of Animal
  Monkey* = ref object of Animal

method vocalize(self: Animal): string {.base.} =
  result = "nd"

method vocalize(self: Dog): string =
  result = "woof"

method vocalize(self: Cat): string =
  result = "meow"

var myDog    = Dog(name: "Bello", gender: Male)
var myCat    = Cat(name: "Kitty", gender: Female)
var myMonkey = Monkey(name: "Flipp", gender: Male)

echo myDog.vocalize()    # "woof"
echo myCat.vocalize()    # "meow"
echo myMonkey.vocalize() # "nd"
