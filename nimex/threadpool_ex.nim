import threadpool

type
  ThreadParams = object
    greet*: string

proc doWork(params: ThreadParams) =
  echo(params.greet)

var params = ThreadParams(greet: "Hello World!")

spawn doWork(params)
