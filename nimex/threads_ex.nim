type
  ThreadParams = object
    greet*: string

proc run(params: ThreadParams) {.thread.} =
  echo(params.greet)

var thread: Thread[ThreadParams]
var params = ThreadParams(greet: "Hello World!")

createThread[ThreadParams](thread, run, params)

joinThread(thread)
